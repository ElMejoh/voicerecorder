package com.example.voicerecorder.ui.main;

import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.voicerecorder.MainActivity;
import com.example.voicerecorder.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private View view;

    private ImageView logo;
    private FloatingActionButton record;
    private FloatingActionButton stop;
    private FloatingActionButton play;

    private String fileName;
    private MediaRecorder mRecorder = null;
    private MediaPlayer mPlayer = null;


    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment, container, false);

        ActivityCompat.requestPermissions(getActivity(), MainActivity.permissions, MainActivity.REQUEST_RECORD_AUDIO_PERMISSION);

        String ruta = getContext().getExternalFilesDir(null).getAbsolutePath();
        fileName = ruta + "/audiorecord.3gp";

        findView();

        play.setOnClickListener(view1 -> {
            playClick();
        });
        stop.setOnClickListener(view1 -> {
            stopClick();
        });
        record.setOnClickListener(view1 -> {
            recordClick();
        });

        return view;
    }

    // PLAY AUDIO
    private void playClick() {
        logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_playing));

        if (mRecorder == null && mPlayer == null) {
            mPlayer = new MediaPlayer();

            try {
                mPlayer.setDataSource(fileName);
                mPlayer.prepare();
                mPlayer.start();

                mPlayer.setOnCompletionListener(mediaPlayer -> {
                    stop.callOnClick();
                });
            } catch (IOException e) {
                Log.e("RECORDING", "No es pot iniciar la reproducció");
            }
        }
    }

    // STOP RECORDING OR PLAYING
    private void stopClick() {
        logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_idle));

        if (mRecorder != null) {
            mRecorder.stop();
            mRecorder.reset();
            mRecorder.release();
            mRecorder = null;
        } else if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
    }

    // START RECORDING
    private void recordClick() {
        logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_recording));

        if (mRecorder == null) {
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setOutputFile(fileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            try {
                mRecorder.prepare();
            } catch (IOException e) {
                Log.e("RECORDING", "No es pot iniciar la gravació");
            }

            mRecorder.start();
        }
    }


    private void findView() {
        logo = view.findViewById(R.id.imgVw_icon);
        record = view.findViewById(R.id.fltActBtn_record);
        stop = view.findViewById(R.id.fltActBtn_stop);
        play = view.findViewById(R.id.fltActBtn_play);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);


    }


}